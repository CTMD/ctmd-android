# Licenses
[GPL-3.0](https://gitlab.com/CTMD/ctmd-android/blob/master/LICENSE)
## Third-party License
[jsoup](https://github.com/jhy/jsoup) by [Jonathan Hedley](https://github.com/jhy) under [MIT](https://github.com/jhy/jsoup/blob/master/LICENSE)

[AndroidPdfViewer](https://github.com/barteksc/AndroidPdfViewer) by [barteksc](https://github.com/barteksc) under [Apache-2.0](https://github.com/barteksc/AndroidPdfViewer/blob/master/LICENSE)

[Picasso](https://github.com/square/picasso) by [Square](https://github.com/square) under [Apache-2.0](https://github.com/square/picasso/blob/master/LICENSE.txt)