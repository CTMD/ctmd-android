package com.gitlab.ctmd;

import android.Manifest;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import java.io.File;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;

public class StudyActivity extends MainActivity  {

    private DownloadManager downloadManager;
    private long refid;
    private Uri Download_Uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));


        Download_Uri = Uri.parse("https://www.tutorialspoint.com/android/android_tutorial.pdf");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_study);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        File extStore = Environment.getExternalStorageDirectory();
        File file = new File(extStore.getAbsolutePath() + "/Download/carte_ctmd.pdf");
        if(file.exists()){
            pdfView();
        } else {
            downloadPDF();
        }
    }

    public void pdfView() {
        ActivityCompat.requestPermissions(StudyActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                2);
        File extStore = Environment.getExternalStorageDirectory();
        File file = new File(extStore.getAbsolutePath() + "/Download/carte_ctmd.pdf");
        PDFView pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromFile(file).load();
    }

    public void downloadPDF() {
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("carte_ctmd" + ".pdf");
        request.setDescription("carte_ctmd" + ".pdf");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "carte_ctmd" + ".pdf");

        refid = downloadManager.enqueue(request);
    }

    @Override
    protected int getItemId() {
        return NAVDRAWER_ITEM_C;
    }


    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(StudyActivity.this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Dependencies")
                            .setContentText("PDF file downloaded");

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(455, mBuilder.build());
            pdfView();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }
}
