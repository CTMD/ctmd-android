package com.gitlab.ctmd;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private final ArrayList<Event> eventArrayList;

    public EventsAdapter(ArrayList<Event> eventArrayList) {
        this.eventArrayList = eventArrayList;
    }

    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view, viewGroup, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder eventsViewHolder, int i) {
        final Event event = eventArrayList.get(eventsViewHolder.getAdapterPosition());

        eventsViewHolder.eventTitle.setText(event.getTitle());
        eventsViewHolder.eventLink.setText(event.getLink());
        Picasso.get().load(eventArrayList.get(i).getImg())
                .fit().centerCrop().into(eventsViewHolder.eventImg);

        eventsViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), BrowseActivity.class);
                intent.putExtra("link", event.getLink());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventArrayList.size();
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder {
        private final TextView eventTitle;
        private final TextView eventLink;
        private final ImageView eventImg;
        private final View view;

        EventsViewHolder(View itemView) {
            super(itemView);
            eventTitle = (TextView) itemView.findViewById(R.id.eventTitle);
            eventLink = (TextView) itemView.findViewById(R.id.eventLink);
            eventImg = (ImageView) itemView.findViewById(R.id.eventImg);
            view = itemView.findViewById(R.id.cv);
        }
    }
}
