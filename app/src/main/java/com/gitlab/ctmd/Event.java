package com.gitlab.ctmd;

public class Event {
    private String img, title, link;

    public Event() {

    }

    public Event(String img, String title, String link) {
        this.img = img;
        this.title = title;
        this.link = link;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
