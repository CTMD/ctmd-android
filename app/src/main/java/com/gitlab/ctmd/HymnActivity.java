package com.gitlab.ctmd;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.SQLException;

public class HymnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hymn);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_hymn);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Set initially id
        long userId=0;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // Get id from clicked ListView item
            userId = extras.getLong("id");
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, PlaceholderFragment.newInstance(userId))
                    .commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // A placeholder fragment containing a simple view.
    public static class PlaceholderFragment extends Fragment {

        TextView title, lyrics;
        DatabaseHelper sqlHelper;
        Cursor userCursor;

        public static PlaceholderFragment newInstance(long id) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args=new Bundle();
            args.putLong("id", id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_hymn, container, false);
            title = (TextView) rootView.findViewById(R.id.hymnTitle);
            lyrics = (TextView) rootView.findViewById(R.id.hymnLyrics);
            lyrics.setMovementMethod( new ScrollingMovementMethod());

            final long id = getArguments() != null ? getArguments().getLong("id") : 0;
            sqlHelper = new DatabaseHelper(getActivity());
            try {
                // Open database with method from "DatabaseHelper" class
                sqlHelper.open();
                if (id > 0) {
                    // Obtain an element with respect to id from δδ
                    userCursor = sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE + " where " +
                            DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(id)});
                    userCursor.moveToFirst();
                    // Set data for elements from activity (fragment_enterprise)
                    title.setText(userCursor.getString(1));
                    lyrics.setText(String.valueOf(userCursor.getString(2)));
                    // Close the cursor
                    userCursor.close();
                } else {

                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
            }
            return rootView;
        }
    }
}
