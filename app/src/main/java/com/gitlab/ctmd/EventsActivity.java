package com.gitlab.ctmd;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class EventsActivity extends MainActivity {

    private String CTMD_LOG = "log";
    private ArrayList<Event> eventArrayList = new ArrayList<>();
    private EventsAdapter adapter;
    private RecyclerView recyclerView;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_events);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        GridLayoutManager glm =new GridLayoutManager(this,1);
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setLayoutManager(glm);

        new Parse().execute();
    }

    class Parse extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressDialog
            mProgressDialog = new ProgressDialog(EventsActivity.this);
            // Set progressDialog title
            mProgressDialog.setTitle("Va rugam, asteptati");
            // Set progressDialog message
            mProgressDialog.setMessage("Se descarca datele...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            // Show progressDialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String webUrl = "http://tineret.adventist.md/1176-2/";

            try {
                String img, title, link;

                Document doc = Jsoup.connect(webUrl).get();
                Elements elements = doc
                        .select(".item-event-future");
                for (Element element : elements) {
                    img = element
                            .select("img")
                            .attr("src");
                    title = element
                            .select(".ief-title").text();
                    link = element
                            .select("a")
                            .attr("href");

                    img = img.replace("?fit=100%2C67", "")
                            .replace("?fit=100%2C65", "");

                    Log.d(CTMD_LOG, img + " " + title + " " + link);
                    Event event = new Event(img, title, link);
                    eventArrayList.add(event);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(CTMD_LOG, String.valueOf(eventArrayList.size()));

            adapter = new EventsAdapter(eventArrayList);
            recyclerView.setAdapter(adapter);
            // Close the progressDialog
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected int getItemId() {
        return NAVDRAWER_ITEM_B;
    }
}
