package com.gitlab.ctmd;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;
public abstract class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigator;

    private Handler handler;

    private boolean close = false;

    // CONSTANTS
    static final int NAVDRAWER_ITEM_A = R.id.nav_item_a;
    static final int NAVDRAWER_ITEM_B = R.id.nav_item_b;
    static final int NAVDRAWER_ITEM_C = R.id.nav_item_c;
    static final int NAVDRAWER_ITEM_D = R.id.nav_item_d;
    private static final int NAVDRAWER_ITEM_INVALID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        drawer = (DrawerLayout) findViewById(R.id.drawer);
        navigator = (NavigationView) findViewById(R.id.navigation);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        navigator.setCheckedItem(getItemId());
        navigator.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            openNavDrawer();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            manageCloseEvent();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        if (item.getItemId() == getItemId()) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int selectedId = item.getItemId();
            navigator.setCheckedItem(selectedId);
            drawer.closeDrawer(GravityCompat.START);
            performScreenChange(selectedId);
        }
        return true;
    }

    int getItemId() {
        return NAVDRAWER_ITEM_INVALID;
    }

    private boolean isNavDrawerOpen() {
        return drawer != null && drawer.isDrawerOpen(GravityCompat.START);
    }

    private void openNavDrawer() {
        if (drawer != null) drawer.openDrawer(GravityCompat.START);
    }

    private void closeNavDrawer() {
        if (drawer != null) drawer.closeDrawer(GravityCompat.START);
    }

    private void performScreenChange(final int item) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                changePage(item);
            }
        }, 250);
    }

    private void changePage(int pos) {
        switch (pos) {
            case R.id.nav_item_a: startActivity(new Intent(this, HymnsActivity.class)); break;
            case R.id.nav_item_b: startActivity(new Intent(this, EventsActivity.class)); break;
            case R.id.nav_item_c: startActivity(new Intent(this, StudyActivity.class)); break;
            case R.id.nav_item_d: startActivity(new Intent(this, AboutActivity.class)); break;
        }

        overridePendingTransition(R.anim.change, R.anim.change);
        finish();
    }

    private void manageCloseEvent() {
        if (close) super.onBackPressed();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                close = false;
            }
        }, 1500);

        Toast.makeText(this, getString(R.string.message_close_app), Toast.LENGTH_SHORT).show();
        close = true;
    }
}
