package com.gitlab.ctmd;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;

import java.io.File;
import java.sql.SQLException;

public class HymnsActivity extends MainActivity {

    EditText hymnsFilter;
    ListView hymnsList;

    DatabaseHelper sqlHelper;
    Cursor hymnsCursor;
    SimpleCursorAdapter hymnsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hymns);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_hymns);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        hymnsFilter = (EditText)findViewById(R.id.hymnsFilter);
        hymnsList = (ListView)findViewById(R.id.hymnsList);

        sqlHelper = new DatabaseHelper(getApplicationContext());
        sqlHelper.create_db();

        // React to user clicks on item
        hymnsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), HymnActivity.class);
                /* Get id for clicked items
                 * Start activity (HymnActivity) */
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getItemId() {
        return NAVDRAWER_ITEM_A;
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            // Open connection with database
            sqlHelper.open();
            // Get data from database in cursor form
            hymnsCursor = sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE, null);
            // Determine which columns from the cursor will be displayed in the ListView
            String[] headers = new String[]{DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_TITLE};
            // Create an adapter, send the cursor to it
            hymnsAdapter = new SimpleCursorAdapter(this, R.layout.hymns_listview_item,
                    hymnsCursor, headers, new int[]{R.id.number_item, R.id.title_item}, 0);
            // If there is text in the text field, perform the filtering
            if(!hymnsFilter.getText().toString().isEmpty())
                hymnsAdapter.getFilter().filter(hymnsFilter.getText().toString());
            // Setting change listener
            hymnsFilter.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                // When changing the text, perform filtering
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    hymnsAdapter.getFilter().filter(s.toString());
                }
            });
            // Determines the column number
            hymnsAdapter.setStringConversionColumn(1);
            // Install the filtering provider
            hymnsFilter.setHint("Cauta dupa denumire");
            hymnsAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                @Override
                public Cursor runQuery(CharSequence constraint) {
                    if (constraint == null || constraint.length() == 0) {
                        return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE, null);
                    }
                    else {
                        return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE + " where " +
                                DatabaseHelper.COLUMN_TITLE + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                }
            });
            Switch switchFilter = (Switch) findViewById(R.id.switchFilter);
            switchFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        hymnsFilter.setHint("Cauta dupa numar");
                        hymnsAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                            @Override
                            public Cursor runQuery(CharSequence constraint) {
                                if (constraint == null || constraint.length() == 0) {
                                    return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE, null);
                                }
                                else {
                                    return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE + " where " +
                                            DatabaseHelper.COLUMN_ID + " like ?", new String[]{"%" + constraint.toString() + "%"});
                                }
                            }
                        });
                    } else {
                        hymnsFilter.setHint("Cauta dupa denumire");
                        hymnsAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                            @Override
                            public Cursor runQuery(CharSequence constraint) {
                                if (constraint == null || constraint.length() == 0) {
                                    return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE, null);
                                }
                                else {
                                    return sqlHelper.database.rawQuery("select * from " + DatabaseHelper.TABLE + " where " +
                                            DatabaseHelper.COLUMN_TITLE + " like ?", new String[]{"%" + constraint.toString() + "%"});
                                }
                            }
                        });
                    }
                }
            });
            // Set adapter for ListView
            hymnsList.setAdapter(hymnsAdapter);
        } catch (SQLException ex) {}
    }

    @Override
    public void onDestroy(){
        // Close connection with database
        super.onDestroy();
        sqlHelper.database.close();
        hymnsCursor.close();
    }
}
