package com.gitlab.ctmd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Declares the package of database
    private static String DB_PATH = "/data/data/com.gitlab.ctmd/databases/";
    // Declares the name of database
    private static String DB_NAME = "hymns.db";
    // Declares the version of database
    private static final int SCHEMA = 1;
    // Declares the name of table
    static final String TABLE = "hymns";

    // Declares the fields name of table
    static final String COLUMN_ID = "_id";
    static final String COLUMN_TITLE = "title";
    static final String COLUMN_LYRICS = "lyrics";

    public SQLiteDatabase database;
    private Context myContext;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, SCHEMA);
        this.myContext=context;
        DB_PATH =context.getFilesDir().getPath() + DB_NAME;
    }
    // Called when the database is created for the first time
    @Override
    public void onCreate(SQLiteDatabase db) {

    }


    // Method for creating database
    public void create_db(){
        InputStream myInput = null;
        OutputStream myOutput = null;
        try {
            File file = new File(DB_PATH + DB_NAME);
            if (!file.exists()) {
                // Set database as readable
                this.getReadableDatabase();
                // Obtain a local database
                myInput = myContext.getAssets().open(DB_NAME);
                // Set directory of new database
                String outFileName = DB_PATH + DB_NAME;
                myOutput = new FileOutputStream(outFileName);
                // Copy data
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }
                myOutput.flush();
                myOutput.close();
                myInput.close();
            }
        }
        catch(IOException ex){

        }
    }

    public void open() throws java.sql.SQLException {
        String path = DB_PATH + DB_NAME;
        database = SQLiteDatabase.openDatabase(path, null,
                SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
        }
        super.close();
    }
    // Called when the database needs to be upgraded
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
